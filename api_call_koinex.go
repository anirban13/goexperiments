package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func koinex_data() (string, []interface{}, error) {

	var url string = "https://koinex.in/api/ticker"

	req, err := http.NewRequest("GET", url, nil) // http.NewRequest returns 2 values but only 1st one is req
	if err != nil {
		return "", nil, err
	}
	// fmt.Println(req)

	res, err := http.DefaultClient.Do(req) // same as above

	if err != nil {
		return "", nil, err
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body) // same as above
	// fmt.Println(body)

	if err != nil {
		return "", nil, err
	}

	// jsonBody := string(body)

	temp := make(map[string]interface{})
	mapString := make(map[string]string)
	mapInterface := make([]interface{}, getLen(temp))
	err = json.Unmarshal([]byte(body), &temp)
	if err != nil {
		return "", nil, err
	}
	for key, value := range temp {
		strKey := fmt.Sprintf("%v", key)
		strValue := fmt.Sprintf("%v", value)

		mapString[strKey] = strValue
	}
	fmt.Println(getLen(temp))
	fmt.Println(mapString)
	var i int
	for value, _ := range temp {
		// strKey := fmt.Sprintf("%v", key)
		strValue := fmt.Sprintf("%v", value)

		mapInterface[i] = strValue
		fmt.Println(strValue, "\n\n")
		i = i + 1
	}

	// jsonBody = json.Marshal(temp)
	return createKeyValuePairs(mapString), mapInterface, nil
	// return jsonBody, nil

}

func createKeyValuePairs(m map[string]string) string {
	b := new(bytes.Buffer)
	for key, value := range m {
		fmt.Fprintf(b, "%s=\"%s\"\n", key, value)
	}
	return b.String()
}

func getLen(m map[string]interface{}) int64 {
	var i int64
	i = 1
	for val, _ := range m {
		for range val {
			i = i + 1
		}

	}
	return i
}
