package main

import (
	"context"
	"testing"

	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/mongo"
	"github.com/stretchr/testify/require"
)

func requireCursorLength(t *testing.T, cursor mongo.Cursor, length int) {
	i := 0
	for cursor.Next(context.Background()) {
		i++
	}

	require.NoError(t, cursor.Err())
	require.Equal(t, i, length)
}

func InsertExamples(t *testing.T, db *mongo.Database, d []interface{}) {
	_, err := db.RunCommand(
		context.Background(),
		bson.D{{"dropDatabase", 1}},
	)

	require.NoError(t, err)

	coll := db.Collection("coindata")

	{
		// Start Example 1

		// result, _ := coll.InsertOne(
		// 	context.Background(),
		// 	bson.D{
		// 		{"item", "canvas"},
		// 		{"qty", 100},
		// 		{"tags", bson.A{"cotton"}},
		// 		{"size", bson.D{
		// 			{"h", 28},
		// 			{"w", 35.5},
		// 			{"uom", "cm"},
		// 		}},
		// 	})

		result, _ := coll.InsertMany(context.Background(), d)

		// End Example 1

		require.NoError(t, err)
		require.Len(t, result.InsertedIDs, 3)
	}

	// {
	// 	// Start Example 2

	// 	cursor, err := coll.Find(
	// 		context.Background(),
	// 		bson.D{{"item", "canvas"}},
	// 	)

	// 	// End Example 2

	// 	require.NoError(t, err)
	// 	requireCursorLength(t, cursor, 1)

	// }

	// {
	// 	// Start Example 3

	// 	result, err := coll.InsertMany(
	// 		context.Background(),
	// 		[]interface{}{
	// 			bson.D{
	// 				{"item", bsonx.String("journal")},
	// 				{"qty", bsonx.Int32(25)},
	// 				{"tags", bson.A{"blank", "red"}},
	// 				{"size", bson.D{
	// 					{"h", 14},
	// 					{"w", 21},
	// 					{"uom", "cm"},
	// 				}},
	// 			},
	// 			bson.D{
	// 				{"item", bsonx.String("mat")},
	// 				{"qty", bsonx.Int32(25)},
	// 				{"tags", bson.A{"gray"}},
	// 				{"size", bson.D{
	// 					{"h", 27.9},
	// 					{"w", 35.5},
	// 					{"uom", "cm"},
	// 				}},
	// 			},
	// 			bson.D{
	// 				{"item", "mousepad"},
	// 				{"qty", 25},
	// 				{"tags", bson.A{"gel", "blue"}},
	// 				{"size", bson.D{
	// 					{"h", 19},
	// 					{"w", 22.85},
	// 					{"uom", "cm"},
	// 				}},
	// 			},
	// 		})

	// 	// End Example 3

	// 	require.NoError(t, err)
	// 	require.Len(t, result.InsertedIDs, 3)
	// }
}
