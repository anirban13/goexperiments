package main

import (
	"context"
	"fmt"
	"testing"

	"github.com/mongodb/mongo-go-driver/mongo"
	"github.com/stretchr/testify/require"
)

func main() {

	// fmt.Println(koinex_data())
	var t *testing.T
	_, data, _ := koinex_data()
	fmt.Println(data)
	TestDocumentationExamples(t, data)
}

func TestDocumentationExamples(t *testing.T, d []interface{}) {
	client, err := mongo.Connect(context.Background(), "mongodb://localhost:27017", nil)
	require.NoError(t, err)
	fmt.Println(client)
	db := client.Database("test")
	fmt.Println(db)
	InsertExamples(t, db, d)
}
